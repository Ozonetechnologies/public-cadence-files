
👋 Welcome to the OZONE METAVERSE! We built this smart contract and NFT solution to power the OZONE METAVERSE NFT PARCELS and enable true metaverse ownership using NFT technology at scale in partnerhsip with FLOW  

- THE OZONE MARKET is bound to become the world's premiere destination for all things #metaverse.
From purchasing your own parcel to any accessories you may need, the OZONE MARKET is powered by a unique metaverse native marketplace integration which makes it easy to integrate and use as part of the greater metaverse.

We welcome all metaverse projects to reach out to us if interested to leverage our existing infrastructure and engine to build their own metaverse and NFT marketplaces.

When you buy something on the OZONE MARKET you are buying 3 things :
a) True ownership records on the Flow blockchain
b) An awesome user experience wher you can buy with a simple credit card anywhere in the world or using any of the many crypto currencies we support
c) The awesome OZONE METAVERSE technology, enabling you with the highest performing 3D solution in the world, custom built for you so you can do what you love. 



## 🎬 Live Application

Check out the mainnet application on https://market.ozoneuniverse.com 


## ✨ Getting Started
### 1. register your wallet : Just use a secure email address, and let us give baptise you on the blockchain

### 2. Initialize your account: simply click and let us do all the hard work for you on the blockchain

### 3. Start buying and collecting NFT's: shop till your drop! 

### 4. Use your NFT's: The OZONE METAVERSE project will provide quasi instant utility as the platform is well built already. Keep an eye out for ongoing utility features.


## Project Overview





## 😺 What are METAVERSE?

METAVERSE are parcels of virtual worlds which you can use to build the future of the internet: The 3D interactive internet, AKA: the METAVERSE. 
But under the hood they're non-fungible tokens (NFTs) stored on the Flow blockchain.

Items can be purchased from the marketplace with a credit card or supported fungible tokens (coming soon!)
In the future you'll be able to trade them, sell them, or build any business you want on them (leasing, staking, coops, non profit, fundraising and all kinds of media offerings).


## ❓ HOW IS THE OZONE METAVERSE so amazing?

- **100% web based**: So super easy for the entire world to get a metaverse, and as easy for you to promote and get visitors
- **High performance graphics:** PBR, custom lighting, and physics all running at top FPS on any device... you can always feel the ozone magic 
-** Cross chain compatible**: Not limited to a single blockchain. Although you own your NFT's on FLOW, you can interact and leverage your other assets from any wallet (current support for Ethereum and Solana) - You don't have to choose. Get it all and do what you love.
-** Regenerative, positive, and helping the Earth**: The OZONE METAVERSE will always be high quality, super safe, radically inclusive and respectful of all humans on the planet. We also plant trees and develop forests around the world offsetting 100% of carbon footprint. Just making the world a better place. 
- **Privacy and Choice:** You will always be exploring content you love and nothing else. You will always be clearly aware of what content lies ahead and will always know that no one knows you individually to target any marketing based on your personal data

- Ask questions on our telegram: https://t.me/+YBd61D5s-DthYzI5 


🚀 We are here to help you build the new internet. 
